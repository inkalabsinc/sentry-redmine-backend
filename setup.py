#!/usr/bin/env python
"""
sentry-redmine
==================
Creates issues in redmine for each report error.
Since we have use different plugins and doesn't work this plugin
use the email backend :)
:copyright: (c) 2016 by the Inka-Labs, see AUTHORS for more details.
:license: BSD, see LICENSE for more details.
"""

from setuptools import setup


install_requires = [
    'sentry>=6.4.4',
    'python-redmine>=1.0.2',
]

setup(
    name='sentry-redmine-backend',
    version='0.0.2',
    author='Josue Ttito',
    author_email='josue@inka-labs.com',
    url='https://bitbucket.com/inkalabsinc/sentry-redmine-backend/',
    description='A Sentry extension which integrates with Redmine.',
    long_description=__doc__,
    license='BSD',
    zip_safe=False,
    install_requires=install_requires,
    include_package_data=True,
    entry_points={
        'sentry.apps': [
            'redmine = redmine_backend',
        ],
        'sentry.plugins': [
            'redmine = redmine_backend.plugin:RedminePlugin'
        ],
    },
    classifiers=[
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Topic :: Software Development'
    ],
)
