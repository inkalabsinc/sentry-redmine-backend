from django.conf import settings
from django.core.mail.message import sanitize_address
from django.core.mail.backends.smtp import EmailBackend
from django.utils.encoding import force_bytes

from redmine import Redmine
from redmine.exceptions import ResourceError


class EmailRedmineBackend(EmailBackend):
    """
    A wrapper that manages the SMTP network connection with redmine issue
    creation.
    """

    def _send(self, email_message):
        """A helper method that does the actual sending."""
        if not email_message.recipients():
            return False
        from_email = sanitize_address(email_message.from_email,
                                      email_message.encoding)
        recipients = [sanitize_address(addr, email_message.encoding)
                      for addr in email_message.recipients()]
        message = email_message.message()
        charset = message.get_charset().get_output_charset() if message.get_charset() else 'utf-8'
        try:
            self.connection.sendmail(from_email, recipients,
                                     force_bytes(message.as_string(), charset))
        except:
            if not self.fail_silently:
                raise
            return False

        try:
            redmine = Redmine(settings.REDMINE_URL,
                              key=settings.REDMINE_KEY,
                              requests={'verify': False})
            params = {
                'project_id': 'zertifikate',
                'tracker_id': 1,
                'subject': email_message.subject,
                'description': email_message.body,
                'status_id': 1,
                'assigned_to_id': 3,
            }
            redmine.issue.create(**params)
        except ResourceError:
            pass

        return True
